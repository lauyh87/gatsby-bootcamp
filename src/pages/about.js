import React from 'react'
import {Link} from 'gatsby'
import Layout from '../components/layout'

const AboutPage = () => {
    return (
        <div>
            <Layout>
                <h1>About Me</h1>
                <p>Currently I am working as a full stack developer in a financial service company</p>
                <p><Link to="/contact">Want to work with me?</Link></p>
            </Layout>
        </div>
    )
}

export default AboutPage