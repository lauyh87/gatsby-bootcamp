import React from 'react'
import Layout from '../components/layout'
const ContactPage = ()=>{
    return (
        <div>
            <Layout>
                <h1>Contact Page</h1>
                <h2>You can contact me via my email</h2>
                <p>e: richardlyh[at]icloud.com</p>
            </Layout>
        </div>
    )
}

export default ContactPage